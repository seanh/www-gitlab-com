---
layout: job_family_page
title: "Sales Development Representative, Acceleration"
---
## Job Description

As a member of the Sales Development Representative (SDR), Acceleration Team, you will work closely with the Field & Digital Marketing teams to accelerate prospect time-to-value. Proven outbound SDR or marketing experience is a must. You will also need to be well-versed with the DevOps conversations, GitLab value drivers, and account-based marketing (ABM) strategies. In addition to thinking creatively, you’ll need to have exceptional organizational and time management skills. Acceleration SDRs will be responsible for executing while planning and maintaining a roadmap for future campaigns.

### Responsibilities

* Works with the GitLab field, digital, and product marketing teams to create targeted regional campaigns
* Creates outbound campaign content based on the Command of Message (CoM) Framework and GitLab Value Drivers
* Be able to identify where a prospect is in the sales and marketing funnel and nurture appropriately
* Builds Account & Prospect Target Lists based on our ideal customer profile attributes
* Executes targeted campaigns in specified regions
* Schedule qualified meetings for Regional Sales and Sales Development Teams

### Requirements

* 24 months of sales, outbound SDR, or related experience
* Proficient with the CoM framework, GitLab value drivers, and SDR tools
* Understanding of Account-based (ABM), Digital, and Field Marketing concepts
* Exceptional time management and organizational skills
* “Always-on” customer focus
