---
layout: markdown_page
title: "Onboarding"
---

The instructions here are in addition to the on-boarding issue that People Ops will assign on the first day.

Copy the [Quality team onboarding issue template](https://gitlab.com/gitlab-org/quality/team-tasks/blob/master/.gitlab/issue_templates/Onboarding.md)
into a new issue in [QA Team Tasks](https://gitlab.com/gitlab-org/quality/team-tasks/issues/new)
and complete the issue.

## Links

- GitLab QA
  - [Testing Guide / E2E Tests](https://docs.gitlab.com/ee/development/testing_guide/end_to_end)
  - [GitLab QA Orchestrator Documentation](https://gitlab.com/gitlab-org/gitlab-qa/blob/master/README.md)
  - [GitLab QA Testing Documentation](https://gitlab.com/gitlab-org/gitlab-qa/blob/master/docs/README.md)
- Tests
  - [Testing Standards](https://docs.gitlab.com/ee/development/testing_guide/index.html)
- CI infrastructure for CE and EE
  - [Testing from CI](https://docs.gitlab.com/ee/development/testing_guide/ci.html)
- Tests statistics
  - [Redash Test Suite Statistics](https://redash.gitlab.com/dashboard/test-suite-statistics)
- Insights dashboard
  - [Quality Dashboard](http://quality-dashboard.gitlap.com/)
  - [Quality Dashboard Documentation](https://gitlab.com/gitlab-org/gitlab-insights/blob/master/README.md)
- Triage
    - [Triage Onboarding](/handbook/engineering/quality/triage-operations/onboarding/)
- QA Runners
  - [QA Runner Ownership Issue](https://gitlab.com/gitlab-org/gitlab-qa/issues/261)
- Projects
  - [gitlab-org](https://gitlab.com/gitlab-org)
  - [gitlab-com](https://gitlab.com/gitlab-com)
  - [dev.gitlab.org](https://dev.gitlab.org)
  - [staging.gitlab.com](https://staging.gitlab.com)
  - [triage-ops](https://gitlab.com/gitlab-org/quality/triage-ops)

### Slack channels

These internal Slack channels may be helpful to join.

#### Department related

- [#quality](https://gitlab.slack.com/messages/C3JJET4Q6) - general department channel
- [#triage](https://gitlab.slack.com/messages/C39HX5TRV) - channel to aid in [triaging unlabelled issues](/handbook/engineering/quality/triage-operations/#newly-created-unlabelled-issues-requiring-first-triage)
- [#qa-nightly](https://gitlab.slack.com/messages/CGLMP1G7M) - channel with semi-nightly end to end test results
- [#qa-staging](https://gitlab.slack.com/messages/CBS3YKMGD) - channel with daily staging end to end test results
- [#qa-performance](https://gitlab.slack.com/messages/CH8J9EG49) - channel with performance testing results
- [#triage-automations](https://gitlab.slack.com/messages/CLCKT26JH) - channel for pipeline notifications from engineering productivity automation

#### Company related

- [#product](https://gitlab.slack.com/messages/C0NFPSFA8) - observe and interact with members of the product team
- [#development](https://gitlab.slack.com/messages/C02PF508L) - provides awareness of broken master, environment issues and other development related status items.
- [#is-this-known](https://gitlab.slack.com/messages/CETG54GQ0) - find information about canary failures or bugs
- [#questions](https://gitlab.slack.com/messages/C0AR2KW4B) - ask questions and see other questions (and the answers) from other GitLabbers
- [#thanks](https://gitlab.slack.com/messages/C038E3Q6L) - give and see thanks for the awesomeness that GitLabbers do
- [#peopleops](https://gitlab.slack.com/messages/C0SNC8F2N) - interact with people ops
