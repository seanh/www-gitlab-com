---
layout: markdown_page
title: "CFG.1.04 - Configuration Check Reconciliation: CMDB Control Guidance"
---

## On this page
{:.no_toc}

- TOC
{:toc}

# CFG.1.04 - Configuration Check Reconciliation: CMDB

## Control Statement

GitLab reconciles the established device inventory against the enterprise log repository quarterly; devices which do not forward security configurations are remediated.

## Context

This control helps to close the loop between device inventory information and production logs. If all production systems are sending the appropriate logs, there should be a parity between the device inventory GitLab collects and the logs generated from those systems. This control is meant to be a check on the "Configuration Check" control. This reconciliation ensures that all systems that should be forwarding security configuration information, are.

## Scope

This control applies to all production systems.

## Ownership

* Control Owner: `IT Ops`
* Process owner(s): 
    * IT Ops: `100%`

## Guidance

Usually this can be automated by setting up checks that look for logs from key directories (e.g. /var/log/) or key processes (e.g. auditd) and ensuring these logs are present for all systems within the device inventory

## Additional control information and project tracking

Non-public information relating to this security control as well as links to the work associated with various phases of project work can be found in the [Configuration Check Reconciliation: CMDB control issue](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/issues/787).

## Framework Mapping

* SOC2
  * CC6.1
