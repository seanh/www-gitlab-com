---
layout: markdown_page
title: "SLC.1.01 - Service Lifecycle Workflow Control Guidance"
---

## On this page
{:.no_toc}

- TOC
{:toc}

# SLC.1.01 - Service Lifecycle Workflow

## Control Statement

Major software releases are subject to the Service Life Cycle, which requires acceptance via Concept Accept and Project Plan Commit phases prior to implementation.

## Context

The purpose of this control is to formalize the documentation and approval of software changes before those changes are implemented. This rigid process helps protect GitLab from insecure code being quickly pushed out into production without proper vetting.

## Scope

This control applies to all major software releases to GitLab's SaaS offering.

## Ownership

TBD

## Guidance

Most of this process is already captured in current GitLab workflow; the difficult part of this process will be 100% coverage of all software changes.

## Additional control information and project tracking

Non-public information relating to this security control as well as links to the work associated with various phases of project work can be found in the [Service Lifecycle Workflow control issue](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/issues/888).

## Framework Mapping

* ISO
  * A.14.1.1
  * A.14.2.5
* SOC2 CC
  * CC8.1
* PCI
  * 6.3
