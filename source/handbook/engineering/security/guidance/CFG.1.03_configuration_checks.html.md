---
layout: markdown_page
title: "CFG.1.03 - Configuration Checks Control Guidance"
---

## On this page
{:.no_toc}

- TOC
{:toc}

# CFG.1.03 - Configuration Checks

## Control Statement

GitLab uses mechanisms to detect deviations from baseline configurations in production environments.

## Context

This is simply a control that ensures we are monitoring our systems to ensure that configuration standards are actually being applied to all production systems.

## Scope

This control applies to all systems within our production environment.

## Ownership

* Control Owner: `Infrastructure Team`
* Process owner(s):
    * Infrastructure Team: `100%`

## Additional control information and project tracking

Non-public information relating to this security control as well as links to the work associated with various phases of project work can be found in the [Configuration Checks control issue](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/issues/786).

## Framework Mapping

* ISO
  * A.9.4.4
  * A.12.5.1
* SOC2 CC
  * CC6.1
  * CC7.1
  * CC7.2
* PCI
  * 1.2.2
  * 10.4.2
  * 11.4
  * 11.5
  * 11.5.1
  * 5.3
