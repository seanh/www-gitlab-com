---
layout: markdown_page
title: "IAM.2.01 - Unique Identifiers Control Guidance"
---

## On this page
{:.no_toc}

- TOC
{:toc}

#  IAM.2.01 - Unique Identifiers

## Control Statement

GitLab requires unique identifiers for user accounts and prevents identifier reuse.

## Context

An unique identifier allows for every user's action to be logically separated and uniquely identified. It helps use detected and prevent abuse and suspicious activity.

## Scope

An unique identifier (UID) is a numeric or alphanumeric string that is associated with a single entity within a given system. UIDs make it possible to address that entity, so that it can be accessed and interacted with

## Ownership

Control Owner: Security Team

Process owner(s):
* Security Team: 50%
* Business Ops Team: 25%
* Compliance Team: 25%

## Guidance

The access security user ID is most often the only basis for user authentication and authorization, so it is critical that every user has his or her own unique user ID. No two users should have the same user ID. It is vital that no two users be assigned the same user ID. If the uniqueness of user IDs is not assured, some users may be able to access information that they have not been given permission to access.

## Additional control information and project tracking

Non-public information relating to this security control as well as links to the work associated with various phases of project work can be found in the [Unique Identifiers control issue](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/issues/812).

## Framework Mapping

* ISO
  * A.9.4.1
  * A.9.4.2
* SOC2 CC
  * CC6.1
* PCI
  * 8.1.1
  * 8.6
