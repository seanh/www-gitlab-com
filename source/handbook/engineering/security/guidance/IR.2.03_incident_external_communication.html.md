---
layout: markdown_page
title: "IR.2.03 - Incident External Communication Control Guidance"
---

## On this page
{:.no_toc}

- TOC
{:toc}

# IR.2.03 - Incident External Communication

## Control Statement

GitLab communicates a response to external stakeholders as required by the Incident Response Plan.

## Context

This control demonstrates that we can provide evidence of communication in the event of an incident to external stakeholders.

## Scope

TBD

## Ownership

* Control Owner: `Security`
* Process owner(s):
    * Security: `50%`
    * Infrastructure: `50%`

## Guidance

Provide a copy of external communication from an incident in the past 12 months.  If there wasn't an incident, provide evidence that we did not have an incident.

## Additional control information and project tracking

Non-public information relating to this security control as well as links to the work associated with various phases of project work can be found in the [Incident External Communication control issue](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/issues/844).

## Framework Mapping

* PCI
  * 12.10.1
