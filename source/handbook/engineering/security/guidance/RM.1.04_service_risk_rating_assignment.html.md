---
layout: markdown_page
title: "RM.1.04 - Service Risk Rating Assignment Control Guidance"
---

## On this page
{:.no_toc}

- TOC
{:toc}

# RM.1.04 - Service Risk Rating Assignment

## Control Statement

GitLab prioritizes risk management activities based on an assigned service risk rating.

## Context

The purpose of this control is to prioritize security resources and efforts to the services that benefit the most from them. By prioritizing services based on risk rating, we can allocate time and resource to the places that would most benefit from them.

## Scope

This control applies to all GitLab applications and services.

## Ownership

* Control Owner: `Security Compliance`
* Process owner(s):
    * Security: `70%`
    * Security Compliance:`30%`

## Additional control information and project tracking

Non-public information relating to this security control as well as links to the work associated with various phases of project work can be found in the [Service Risk Rating Assignment control issue](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/issues/869).

## Framework Mapping

* SOC2 CC
  * CC1.2
  * CC3.2
  * CC3.4
  * CC4.1
  * CC4.2
  * CC5.1
  * CC5.2
* PCI
  * 12.2
